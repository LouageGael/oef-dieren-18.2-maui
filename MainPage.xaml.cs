﻿namespace Les18._2DierenMaui;

public partial class MainPage : ContentPage
{

    private Dier dier = null;
	public MainPage()
	{
		InitializeComponent();
	}

    private void OnbtnAanmakenClicked(object sender, EventArgs e)
    {
        if(!string.IsNullOrEmpty(txtNaam.Text)) 
        {
            if((bool)rdbKat.IsChecked)
            {
                dier = new Kat(txtNaam.Text);
            } else if((bool)rdbMens.IsChecked)
            {
                dier = new Mens(txtNaam.Text);
            } else if((bool)rdbPapegaai.IsChecked)
            {
                dier = new Papegaai(txtNaam.Text);
            } else
            {
                dier= new Mens(txtNaam.Text);
            }
        
        } else
        {
            txtNaam.Text = "Geef je dier een naam.";
        }
    }
    private void OnbtnEtenClicked(object sender, EventArgs e)
    {
        if(dier != null)
        {
            lbPraten.Text = dier.Eten();
        }
    }
    private void OnbtnPratenClicked(object sender, EventArgs e)
    {

        if(picker.SelectedIndex != -1 && dier != null)
        {
            lbPraten.Text = dier.Praten(picker.SelectedItem.ToString());
        }
    }
    private void OnbtnStrelenClicked(object sender, EventArgs e)
    {
        if (dier != null)
        {
            lbPraten.Text = dier.Strelen();
        }
    }
    private void OnbtnsluitenClicked(object sender, EventArgs e)
    {
        Environment.Exit(0);
    }
}

 class  Dier
{
	private string _naam;
	protected string Naam { get; set; }
	public virtual string Eten() => "";
	public virtual string Praten(string zin) => "";
	public virtual string Strelen() => "";
}
 class Mens : Dier
{
    public Mens(string name)
    {
		Naam = name;
    }
    public override string Eten() => "Lekker!";

    public override string Praten(string zin)
    {
        string result = "";
        switch(zin)
        {
            case "Hallo":
                result = "Hey";
                break;
            case "Hoe gaat het?":
                result = "Alles oké. =)";
                break;
            default:
                result = "...";
                break;
        }
        return result;
    }
    public override string Strelen() => "Blijf van mijn lijf. Arrh.";
}
sealed class Papegaai : Dier
{
    public Papegaai(string name)
    {
        Naam = name;
    }
    public override string Eten() => "Koko";

    public override string Praten(string zin)
    {
        Random random = new Random();
        int randZin = random.Next(1, 5);
        return randZin == 3 ? "Koko kopke krabben.." : zin;

    }
    public override string Strelen() => "Koko";
}
sealed class Kat : Dier
{
    private int _teller = 0;
    public Kat(string name)
    {
        Naam = name;
    }
    public override string Eten()
    {
        return "";
    }

    public override string Praten(string zin)
    {
        _teller++;
        string result = "";
        if (_teller == 3)
        {
            result = "Miauw";
            _teller = 0;
        }
       return result;
    }

    public override string Strelen() => "Rrrr";
}

